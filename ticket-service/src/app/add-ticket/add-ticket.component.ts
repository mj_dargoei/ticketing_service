import { Component, OnInit } from '@angular/core';
import {ApiService} from "../services/api.service";
import {CreateTicket} from "../models/Models";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from '../services/message.service';


@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit {
  ticket:CreateTicket;
  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private msg: MessageService
    ) {

    this.ticket = new CreateTicket();

  }

  ngOnInit(): void {
  }

  createTicket (){
    if (this.ticket.importanceLevel==null
      ||this.ticket.owner==""
      ||this.ticket.subject==""
      ||this.ticket.issuer==""
      ||this.ticket.content==""){
    this.msg.e('Please complete Information')
    }
    else{
      this.apiService.createTicket(this.ticket).subscribe(result=>{
        console.log(result)
      },error => {
        console.log(error)
        this.msg.e(error)

      })
    }

  }

}
