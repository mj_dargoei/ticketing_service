import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FixedPageComponent } from './fixed-page/fixed-page.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { TicketsComponent } from './tickets/tickets.component';
import { CommentComponent } from './comment/comment.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule} from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DpDatePickerModule} from 'ng2-jalali-date-picker';

@NgModule({
  declarations: [
    AppComponent,
    FixedPageComponent,
    FooterComponent,
    HeaderComponent,
    ContentPageComponent,
    TicketsComponent,
    CommentComponent,
    AddTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DpDatePickerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-left',
      preventDuplicates: true,
    }),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
