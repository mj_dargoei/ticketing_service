import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';
import {TicketModel,Comment,CreateTicket,CreateComment} from "../models/Models";


export const apiUrl = environment.apiUrl;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
   // withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  createTicket(data:CreateTicket ) {
    return this.http.post(`${apiUrl}/v1/tickets`, data,httpOptions).pipe(
      catchError(handleError<any>('createTicket')
      ));
  }
  createComment(data:CreateComment ) {
    return this.http.post(`${apiUrl}/v1/comments`, data,httpOptions).pipe(
      catchError(handleError<any>('createComment')
      ));
  }
  filterTicket(data ) {
    let body = <any>{
      owner:data.owner,
      importanceLevel:data.importanceLevel,
      fromDate:data.fromDate,
      toDate:data.toDate,
      pageNumber:data.pageNumber,
      pageSize:data.pageSize
    }
    return this.http.get(`${apiUrl}/v1/tickets?
    owner=${data.owner}
    &importanceLevel=${data.importanceLevel}
    &fromDate=${data.fromDate}
    &toDate=${data.toDate}
    &pageNumber:=${data.pageNumber}
    &pageSize=${data.pageSize}`, httpOptions).pipe(
      catchError(handleError<any>('createComment')
      ));
  }
}

function handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    // UtilitesService.log(`${operation} failed: ${error.message}`);

    return throwError(error.error);
  };
}
