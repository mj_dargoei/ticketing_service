import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';



@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private toastr: ToastrService) { }

  success(msg) {
    this.toastr.success(this.normalizeMessage(msg), 'Success', {
      closeButton: true
    });
  }
  e(msg) {
    this.toastr.error(this.normalizeMessage(msg), 'Error', {
      closeButton: true,
      timeOut: 6000
    });
  }
  i(msg) {
    this.toastr.info(this.normalizeMessage(msg), 'Info', {
      closeButton: true
    });
  }
  w(msg) {
    this.toastr.warning(this.normalizeMessage(msg), 'Warning', {
      closeButton: true
    });
  }
  normalizeMessage(msg) {
    if (!String.prototype.isPrototypeOf(msg)) {
      let tmp;
      try {
        tmp = JSON.stringify(msg);
      } catch (e) {
        tmp = '' + msg;
      }
      msg = tmp;
    }
    return msg;
  }
}
