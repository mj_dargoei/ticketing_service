export class TicketModel {
  ID?:number
  issuer:string
  owner:string
  subject:string
  content:string
  metadata:string
  importanceLevel:string
  status?:number
  comments:Comment[]
  createdAt?:string
  modifiedAt?:string
}
export class CreateTicket {
  issuer:string
  owner:string
  subject:string
  content:string
  metadata:string
  importanceLevel:string
}
export class Comment {
  ID?:number
  ticketID:number
  owner:string
  content:string
  metadata:string
  createdAt?:string
  modifiedAt?:string
}
export class CreateComment {
  ticketID:number
  owner:string
  content:string
  metadata:string
}
