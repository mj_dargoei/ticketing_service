import { Component, OnInit } from '@angular/core';
import {CreateComment} from "../models/Models";
import {ApiService} from "../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from '../services/message.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  comment:CreateComment;
  private sub: any;
  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private msg: MessageService
  ) {
    this.comment = new CreateComment();
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params=>{
      this.comment.ticketID = params['id'];
    })
    console.log(this.comment.ticketID)
  }
  createComment (){
    if (this.comment.owner==""
    ||this.comment.content==""
    ||this.comment.metadata==""
    ||this.comment.ticketID==null){
      this.msg.e('Please complete Information');
    }
    else {
      this.apiService.createComment(this.comment).subscribe(result=>{

      },error => {
        this.msg.e(error);
      })
    }

  }

}
