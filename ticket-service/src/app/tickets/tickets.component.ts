import { Component, OnInit } from '@angular/core';
import * as moment from 'jalali-moment';
@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {
  dateDefault = new Date();
  day = this.dateDefault.getDay();
  month = this.dateDefault.getMonth();
  year = this.dateDefault.getFullYear();
  fromDate = moment('1399/08/01','jYYYY,jMM,jDD');
  toDate = moment('1399/08/01','jYYYY,jMM,jDD');
  constructor() { }

  ngOnInit(): void {
  }

}
