import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TicketsComponent} from "./tickets/tickets.component";
import {CommentComponent} from "./comment/comment.component";
import {AddTicketComponent} from "./add-ticket/add-ticket.component";


const routes: Routes = [
  { path: '', redirectTo: '/Tickets', pathMatch: 'full' },
  { path: 'Tickets', component:TicketsComponent },
  { path: 'Comment/:id', component:CommentComponent },
  { path: 'Create-Ticket', component:AddTicketComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
